import React from 'react'
import ClientIndex from './components/clients/ClientIndex'
import ContactIndex from './components/contact/ContactIndex'
import ServicesIndex from './components/services/ServicesIndex'
import Avatar from '../../../assets/images/icons/avatar.jpeg'
import CardImage from '../../../assets/images/backgrounds/student-2.jpg';
import ServicesCard from './components/services/ServicesCard'
import Facebook from '../../../assets/images/icons/facebook.png';
import Instagram from '../../../assets/images/icons/instagram.png';
import Twitter from '../../../assets/images/icons/twitter.png';
import Laptop from '../../../assets/images/backgrounds/laptop.jpg'
const serviceItem = {id: 1, avatar: Avatar, img: CardImage, name: 'Diko M', title: 'Graphic Designer'};


function HomepageIndex() {
    return (
        <div className="data-wrap">
            <div className="welcome-contain container-fluid">
                <div className="welcome-wrap">
                    <h1>
                        Welcome To inploy
                    </h1>
                    <p>Register now and secure your spot on top of your category</p>
                </div>
            </div>


            <ServicesIndex />


            <div className="clients-contain">
                <div className="clients-wrap container">
                    <p>Success Stories with 400+ Clients</p>
                    <ClientIndex />
                </div>
            </div>

            <div className="contact-contain">
                <div className="contact-wrap container">
                    <div className="contact-items-wrap d-flex flex-wrap">
                        <div className="contact-item form">
                            <ContactIndex />
                        </div>
                        <div className="contact-item screen">
                            <div className="screen-contain">
                                <div className="screen-bg">
                                    <img src={Laptop} alt="background" />
                                </div>
                                <div className="screen-wrap">
                                    <div className="screen-card">
                                        <ServicesCard 
                                            avatar={serviceItem.avatar} 
                                            image={serviceItem.img} 
                                            name={serviceItem.name} 
                                            title={serviceItem.title} 
                                        />
                                    </div>
                                    <div className="screen-data">
                                        <div className="screen-icon">
                                            <i className="material-icons">insert_comment</i>
                                        </div>
                                        <div className="contact-btn">
                                            <ul>
                                                <li>
                                                    <a href="tel:+123">012-123-12356</a>
                                                </li>
                                                <li>
                                                    <a href="mailto:aa@aa.com">Sohaila-Kandil@mail.com</a>
                                                    <i className="material-icons">keyboard_arrow_left</i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="social-icons-wrap">
                                            <ul>
                                                <li>
                                                    <a href="www.facebook.com">
                                                        <img src={Facebook} alt="icon" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="www.facebook.com">
                                                        <img src={Instagram} alt="icon" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="www.facebook.com">
                                                        <img src={Twitter} alt="icon" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="www.facebook.com">
                                                        <img src={Facebook} alt="icon" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="www.facebook.com">
                                                        <img src={Instagram} alt="icon" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="www.facebook.com">
                                                        <img src={Twitter} alt="icon" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    )
}

export default HomepageIndex
