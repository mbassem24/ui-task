import React from 'react'
import Avatar from '../../../../../assets/images/icons/avatar.jpeg';
import CardImage from '../../../../../assets/images/backgrounds/student-2.jpg';
import ServicesCard from './ServicesCard';
import RegisterForm from './RegisterForm';

const serviceItems = [
    {id: 1, avatar: Avatar, img: CardImage, name: 'Diko M', title: 'Photographer | Videographer | Graphic Designer'},
    {id: 2, avatar: Avatar, img: CardImage, name: 'John Len', title: 'Web Developer'},
    {id: 3, avatar: Avatar, img: CardImage, name: 'Marcos S', title: 'Mobile Developer'},
]
function Provider() {
    return (
        <div className="services-data-contain">
            <div className="services-data-wrap">
                <p>2000+ Services providers on board</p>
                <div className="services-items-wrap d-flex flex-wrap">
                    <div className="services-item data">
                        <div className="services-data">

                            {
                               serviceItems.map((item) => {
                                   return (
                                       <ServicesCard key={item.id} avatar={item.avatar} image={item.img} name={item.name} title={item.title} />
                                   )
                               })
                            }

                            
                        </div>
                    </div>
                    <div className="services-item form">
                        <div className="services-form-wrap">
                            <RegisterForm />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Provider
