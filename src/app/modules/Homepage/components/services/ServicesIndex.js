import React, { useState } from 'react'
import Client from './Client';
import Provider from './Provider';

function ServicesIndex() {
    const [activeTab, setActiveTab] = useState('provider');

    const handleTab = (item) => {
        setActiveTab(item)
    }


    return (
        <div className="services-contain">
            <div className="services-wrap container">
                <div className="services-head">
                    <div className="tabs-contain">
                        <ul>
                            <li className={`${activeTab === 'provider' ? 'active' : ''}`} onClick={() => handleTab('provider')}>Service Provider</li>
                            <li className={`${activeTab === 'client' ? 'active' : ''}`} onClick={() => handleTab('client')}>Client</li>
                        </ul>
                    </div>
                </div>
                <div className="services-body">
                    {
                        activeTab === 'provider'
                        ?   <Provider />
                        :   <Client />
                    }
                </div>

                <div className="services-foot">
                    <ul>
                        <li>
                            <i className="material-icons">grade</i>
                            <span>Free Commission</span>
                        </li>

                        <li>
                            <i className="material-icons">autorenew</i>
                            <span>Free Contact Exchange</span>
                        </li>

                        <li>
                            <i className="material-icons">assignment</i>
                            <span>30+ Services</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default ServicesIndex
