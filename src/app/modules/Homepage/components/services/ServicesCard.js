import React from 'react'
import PrevArrow from '../../../../../assets/images/icons/prev-arrow-white.png'
import NextArrow from '../../../../../assets/images/icons/next-arrow-white.png'

function ServicesCard({image, avatar, name, title}) {
    return (
        <div className="services-card">
            <div className="card-img">
                <img src={image} className="main" alt="background" />
                <div className="static-arrow">
                    <img src={PrevArrow} alt="background" />
                    <img src={NextArrow} alt="background" />
                </div>
            </div>
            <div className="card-data">
                <div className="card-head">
                    <ul>
                        <li className="card-status">
                            <span className="material-icons">done</span>
                        </li>
                        <li className="user-avatar">
                            <img src={avatar} alt="user" />
                        </li>
                        <li className="card-rate">
                            <span>5.0</span>
                        </li>
                    </ul>
                </div>
                <div className="card-body">
                    <h3 className="user-name">{name}</h3>
                    <p className="user-title">{title}</p>
                    <div className="view-btn">
                        <span>Quick View</span>
                        <i className="material-icons">keyboard_arrow_down</i>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServicesCard
