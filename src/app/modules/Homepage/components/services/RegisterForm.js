import React, { useState } from 'react'


function RegisterForm() {
    const [formData, setFormData] = useState({
        fullName: '',
        jobTitle: '',
        email: '',
        phoneNumber: '',
        category: ''
    })


    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value})
    }


    const handleFormSubmit = (e) => {
        e.preventDefault()
        console.log(formData)
    }
    return (
        <div className="form-wrap">
            <div className="form-head">
                <h3>Register Now</h3>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Full Name"
                                name="fullName"
                                value={formData.fullName}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Job Title"
                                name="jobTitle"
                                value={formData.jobTitle}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <input
                                type="email"
                                className="form-control"
                                placeholder="Email"
                                name="email"
                                value={formData.email}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <input
                                type="number"
                                className="form-control"
                                placeholder="Number"
                                name="phoneNumber"
                                value={formData.phoneNumber}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group customSelect">
                            <select className="form-control" value={formData.category} name='category' onChange={handleChange} placeholder="category">
                                <option value="Category">Category</option>
                                <option value="Category 2">Category 2</option>
                                <option value="Category 3">Category 3</option>
                            </select>

                            <div className="select-arrow">
                                <i className="material-icons">keyboard_arrow_down</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-submit mt-3">
                    <button className="btn btn-primary">Register Now</button>
                </div>
            </form>
        </div>
    )
}

export default RegisterForm
