import React, { useState } from 'react'

function ContactIndex() {
    const [formData, setFormData] = useState({
        email: '',
        phoneNumber: '',
    })
    const [contactChoice, setContactChoice] = useState('call')


    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value})
    }


    const handleFormSubmit = (e) => {
        e.preventDefault()
        console.log(formData)
    }

    const handleRadioChange = (e) => {
        setContactChoice(e.target.value)
    }
    return (
        <div className="form-wrap">
            <div className="form-head">
                <h3>
                    Contact <br />
                    info@inploy.me
                </h3>
                <p>For further assistance with projects or registeration</p>
            </div>
            <form onSubmit={handleFormSubmit}>
                <div className="form-row">

                    <div className="col-md-12">
                        
                        
                        <div className="form-check-wrap">
                            <strong>How would like us to contact you?</strong>
                            <div className="form-check">
                                <input 
                                    type="radio" 
                                    id="call" 
                                    name="contact" 
                                    value="call"
                                    className="form-check-input"
                                    defaultChecked
                                    onChange={handleRadioChange}
                                />
                                <label className="form-check-label" htmlFor="call">Call</label>
                            </div>
                            <div className="form-check">
                                <input 
                                    type="radio" 
                                    id="email" 
                                    name="contact"
                                    value="email"
                                    className="form-check-input"
                                    onChange={handleRadioChange}
                                />
                                <label className="form-check-label" htmlFor="email">Email</label>
                            </div>
                        </div>
                    
                    
                    </div>
                    
                    
                    {
                        contactChoice === 'call'
                        ?   <div className="col-md-12">
                                <div className="form-group">
                                    <input
                                        type="number"
                                        className="form-control"
                                        placeholder="Number"
                                        name="phoneNumber"
                                        value={formData.phoneNumber}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                        
                        :   <div className="col-md-12">
                                <div className="form-group">
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Email"
                                        name="email"
                                        value={formData.email}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                    }
                    
                    
                    
                </div>
                <div className="form-submit mt-3">
                    <button className="btn btn-primary">Send</button>
                </div>
            </form>
        </div>
    )
}

export default ContactIndex
