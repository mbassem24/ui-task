import React from 'react'
import Icon1 from '../../../../../assets/images/clients/aquavia.jpg';
import Icon2 from '../../../../../assets/images/clients/aquavia-1.jpg';
import Icon3 from '../../../../../assets/images/clients/aquavia-2.jpg';
import Icon4 from '../../../../../assets/images/clients/aquavia-3.jpg';
import Icon5 from '../../../../../assets/images/clients/aquavia-4.jpg';
import Icon6 from '../../../../../assets/images/clients/aquavia-5.jpg';
import Icon7 from '../../../../../assets/images/clients/aquavia-6.jpg';
import Icon8 from '../../../../../assets/images/clients/aquavia-7.jpg';

const clients = [
    {id: 1, icon: Icon1},
    {id: 2, icon: Icon2},
    {id: 3, icon: Icon3},
    {id: 4, icon: Icon4},
    {id: 5, icon: Icon5},
    {id: 6, icon: Icon6},
    {id: 7, icon: Icon7},
    {id: 8, icon: Icon8},
]

function ClientIndex() {
    return (
        <div className="clients-data-contain">
            <div className="clients-data-wrap">
                {
                    clients.map((item) => {
                        return (
                            <div className="client-item" key={item.id}>
                                <img src={item.icon} alt="icon" />
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default ClientIndex
