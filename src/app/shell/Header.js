import React from 'react'

function Header() {
    return (
        <header className="header-contain">
            <div className="header-wrap container">
                <p className="logo">inploy</p>
            </div>
        </header>
    )
}

export default Header
