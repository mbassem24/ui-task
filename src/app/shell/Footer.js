import React from 'react'
import Facebook from '../../assets/images/icons/facebook.png';
import Instagram from '../../assets/images/icons/instagram.png';
import Twitter from '../../assets/images/icons/twitter.png';

function Footer() {
    return (
        <footer className="footer-contain">
            <div className="footer-top">
                <div className="container">
                    <div className="footer-top-in">
                        <div className="footer-logo">
                            <strong>inploy</strong>
                        </div>
                        <div className="social-icons-wrap">
                            <ul>
                                <li>
                                    <a href="www.facebook.com">
                                        <img src={Facebook} alt="icon" />
                                    </a>
                                </li>
                                <li>
                                    <a href="www.facebook.com">
                                        <img src={Instagram} alt="icon" />
                                    </a>
                                </li>
                                <li>
                                    <a href="www.facebook.com">
                                        <img src={Twitter} alt="icon" />
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div className="footer-bot">
                <div className="container">
                    <div className="footer-bot-in">
                        <ul className="footer-links">
                            <li>
                                <p>Support</p>
                            </li>

                            <li>
                                <p>Understand</p>
                            </li>

                            <li>
                                <p>Terms & Regulations</p>
                            </li>
                        </ul>

                        <p className="copy-rights">
                            Inploy 2020. All rights reserved
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer
