import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import HomepageIndex from './app/modules/Homepage/HomepageIndex'
import ScrollReverse from './app/shell/ScrollReverse'
import Header from './app/shell/Header';
import Footer from './app/shell/Footer';

function App() {
    return (

        
        <Router>
            <ScrollReverse />
            
            <Header />
            <Switch>
                <Route exact path="/" component={HomepageIndex} />
                
            </Switch>
            <Footer />

        </Router>
    )
}

export default App


